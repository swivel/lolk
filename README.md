# lolk

`lolk` attempts to resolve ambiguous string-based values by interpreting and
translating them into their intended values. Most of these are difficult to
interpret, given their ability to appear to resolve `true` when `false`, and
vice-versa. `lolk` attempts to solve this problem by implementing a system to
account for a wide-range of false-positive values like `"k."`, while
maintaining the ability to translate literal values like "Sute!" to their
intended literals.


## Usage Example

```javascript
lolk.from("lol k."); // false
lolk.from("sure."); // false
lolk.from("Sure!"); // true
lolk.to(false); // "ha k"
lolk.to(false); // "k."
```
